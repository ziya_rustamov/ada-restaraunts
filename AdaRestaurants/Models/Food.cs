namespace AdaRestaurants.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Food")]
    public partial class Food
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Food()
        {
            Images = new HashSet<Image>();
            OrderFoods = new HashSet<OrderFood>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string FoodName { get; set; }

        [Column(TypeName = "money")]
        public decimal FoodPrice { get; set; }

        public DateTime FoodAdddate { get; set; }

        [StringLength(500)]
        public string FoodDescription { get; set; }

        public int? CategoryID { get; set; }

        public int? BrandID { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual Category Category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Image> Images { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderFood> OrderFoods { get; set; }
    }
}
