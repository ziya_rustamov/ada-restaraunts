namespace AdaRestaurants.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Image")]
    public partial class Image
    {
        public int ID { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        public int? FoodId { get; set; }

        public virtual Food Food { get; set; }
    }
}
