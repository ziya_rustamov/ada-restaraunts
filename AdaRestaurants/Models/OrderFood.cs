namespace AdaRestaurants.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderFood")]
    public partial class OrderFood
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int orderID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int foodID { get; set; }

        public int? quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal? price { get; set; }

        public double? sale { get; set; }

        public virtual Food Food { get; set; }

        public virtual Order Order { get; set; }
    }
}
