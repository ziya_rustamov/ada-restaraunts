﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AdaRestaurants.Models;
namespace AdaRestaurants.App_Classes
{
    public class Context
    {
        private static ModelContext connect;

        public static ModelContext Connect
        {
            get {
                if (connect == null) connect = new ModelContext();
                return connect;
            }
            set { connect = value; }
        }

    }
}