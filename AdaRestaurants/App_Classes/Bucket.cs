﻿using AdaRestaurants.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdaRestaurants.App_Classes
{
    public class Bucket
    {
        public static Bucket ActiveBucket
        {
            get
            {
                HttpContext ctx = HttpContext.Current;
                if (ctx.Session["ActiveBucket"] == null)
                    ctx.Session["ActiveBucket"] = new Bucket();

                return (Bucket)ctx.Session["ActiveBucket"];
            }
        }

        private List<BucketItem> foods = new List<BucketItem>();

        public List<BucketItem> Foods
        {
            get { return foods; }
            set { foods = value; }
        }
        public  void AddBucket(BucketItem bi)
        {
            if(HttpContext.Current.Session["ActiveBucket"] != null)
            {
                Bucket b = (Bucket)HttpContext.Current.Session["ActiveBucket"];
                  if (b.Foods.Any(x => x.Food.ID == bi.Food.ID))
                     {
                b.Foods.FirstOrDefault(x => x.Food.ID == bi.Food.ID).Quantity++;
                      }
                
                    else
                     {
                         b.Foods.Add(bi);
                         
                     } 
                  
            }
            else
            {
                Bucket b = new Bucket();
                b.Foods.Add(bi);
                HttpContext.Current.Session["ActiveBucket"] = b;
            }
            
        }

        public  decimal SumBucket
        {
            get
            {
                return Foods.Sum(x => x.Sum);
            }
        }

    }
    public class BucketItem
    {
        public Food Food { get; set; }
        public int Quantity { get; set; }
        public double Sale{ get; set; }

        public decimal Sum {
            get
            {
                return Food.FoodPrice * Quantity * (decimal)(1 - Sale);
            }
            
        }
    }
}