﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdaRestaurants.App_Classes
{
    public class Images
    {
        public IEnumerable<HttpPostedFileBase> Files { get; set; }
    }
}