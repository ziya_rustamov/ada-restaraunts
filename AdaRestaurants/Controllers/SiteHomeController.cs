﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdaRestaurants.App_Classes;
using AdaRestaurants.Models;

namespace AdaRestaurants.Controllers
{
    public class SiteHomeController : Controller
    {
        // GET: SiteHome
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult Bucket()
        {
            return PartialView();
        }
        public PartialViewResult Slider()
        {
            return PartialView();
        }
        public PartialViewResult NewFoods()
        {

            var data = Context.Connect.Foods.ToList();
            return PartialView(data);
        }
        public PartialViewResult Services()
        {
            return PartialView();
        }
        public PartialViewResult Sales()
        {
            return PartialView();
        }
        public PartialViewResult Brands()
        {
            var data = Context.Connect.Brands.ToList();
            return PartialView(data);
        }
        public void AddToBucket(int id)
        {
            BucketItem bi = new BucketItem();
            Food f = Context.Connect.Foods.FirstOrDefault(x => x.ID == id);
            bi.Food = f;
            bi.Quantity = 1;
            bi.Sale = 0;
            Bucket b = new Bucket();
            b.AddBucket(bi);


            
        }

        public PartialViewResult MiniBucketWidget()
        {
            if (HttpContext.Session["ActiveBucket"] != null)
                return PartialView((Bucket)HttpContext.Session["ActiveBucket"]);
            else return PartialView();
        }

        public void BucketFoodRemove(int id)
        {
            if (HttpContext.Session["Activebucket"] != null)
            {
                Bucket b = (Bucket)HttpContext.Session["ActiveBucket"];
                if (b.Foods.FirstOrDefault(x => x.Food.ID == id).Quantity > 1)
                {
                    b.Foods.FirstOrDefault(x => x.Food.ID == id).Quantity--;
                }
                    
                else
                {
                    BucketItem bi = b.Foods.FirstOrDefault(x => x.Food.ID == id);
                    b.Foods.Remove(bi);
                }
            }
        }

        public ActionResult FoodDetail(string id)
        {
            Food f = Context.Connect.Foods.FirstOrDefault(x => x.FoodName == id);
            return View(f);
        }

        public ActionResult FoodRestaurant(int id)
        {
            var data = Context.Connect.Foods.Where(x => x.BrandID == id).ToList();
            return View(data);

        }
    }
}