﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdaRestaurants.Models;
using System.IO;

namespace AdaRestaurants.Controllers
{
    public class FoodController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Food
        public ActionResult Index()
        {
            var foods = db.Foods.Include(f => f.Brand).Include(f => f.Category);
            return View(foods.ToList());
        }

        // GET: Food/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Food food = db.Foods.Find(id);
            if (food == null)
            {
                return HttpNotFound();
            }
            return View(food);
        }

        // GET: Food/Create
        public ActionResult Create()
        {
            ViewBag.BrandID = new SelectList(db.Brands, "ID", "BrandName");
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "CategoryName");
            return View();
        }

        // POST: Food/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FoodName,FoodPrice,FoodAdddate,FoodDescription,CategoryID,BrandID")] Food food)
        {
            if (ModelState.IsValid)
            {
                db.Foods.Add(food);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BrandID = new SelectList(db.Brands, "ID", "BrandName", food.BrandID);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "CategoryName", food.CategoryID);
            return View(food);
        }

        // GET: Food/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Food food = db.Foods.Find(id);
            if (food == null)
            {
                return HttpNotFound();
            }
            ViewBag.BrandID = new SelectList(db.Brands, "ID", "BrandName", food.BrandID);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "CategoryName", food.CategoryID);
            return View(food);
        }

        // POST: Food/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FoodName,FoodPrice,FoodAdddate,FoodDescription,CategoryID,BrandID")] Food food)
        {
            if (ModelState.IsValid)
            {
                db.Entry(food).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BrandID = new SelectList(db.Brands, "ID", "BrandName", food.BrandID);
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "CategoryName", food.CategoryID);
            return View(food);
        }

        // GET: Food/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Food food = db.Foods.Find(id);
            if (food == null)
            {
                return HttpNotFound();
            }
            return View(food);
        }

        // POST: Food/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Food food = db.Foods.Find(id);
            db.Foods.Remove(food);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AddImg(string id)
        {
            var foodid = Convert.ToInt32(id);
            Food food = db.Foods.Find(foodid);
            return View(food);
        }

        [HttpPost]
        public ActionResult AddImg(App_Classes.Images img, int foodid)
        {
            //her resim için yükleme işlemi yapılsın
            foreach (var file in img.Files)
            {
                if (file.ContentLength > 0)
                {
                    //gelen dosya "Resimler" klasörüne kaydedilsin
                    //gelen dosya adı: var dosyaAdi=Path.GetFileName(dosya.FileName);
                    //her dosya benzersiz bir isme sahip olsun
                    var filename = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    var address = Path.Combine(Server.MapPath("~/Images"), filename);
                    file.SaveAs(address);

                    //gelen dosya "Resim" tablosuna/veritabanına kaydedilsin
                    Image r = new Image();
                    r.FoodId = foodid;
                    r.Name = filename;
                    db.Images.Add(r);
                }
                //yükleme sırasında zaman aşımı süresini artırmak için 
                //<systemWeb> klasörüne aşağıdaki kodu yazınız
                // <httpRuntime maxRequestLength="2097152">
                else
                {
                    ViewBag.Mesaj = "Dosya hatalı! Yükleme işlemi yapılamadı.";
                    return View();
                }
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
